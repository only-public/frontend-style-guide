import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const foo = () => {
    return 1;
    // console.log('test linters'); // example of error and warning // no-unreachable, no-console
  };
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          {foo()}
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
