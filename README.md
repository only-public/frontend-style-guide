Проект основан на [create-react-app](https://github.com/facebook/create-react-app)

Версия NodeJS зафиксирована с помощью [Node Version Manager](https://github.com/nvm-sh/nvm) в “.nvmrc” - это позволяет
не устанавливать NodeJS глобально в системе и параллельно запускать проекты на разных версиях NodeJS.
После установки **nvm** достаточно запустить `nvm use`

В качестве менеджера пакетов используется `Yarn` (на данный момент, classic версия) вместе с [Yarn Workspaces](https://classic.yarnpkg.com/en/docs/workspaces) в связи с тем, что гарантированно работает.
Вы можете мигрировать на актуальную версию [самостоятельно](https://yarnpkg.com/getting-started/migration).
**Yarn Workspaces** позволяет разбивать зависимости проекта по неограниченному количеству дочерних package.json, сгруппированных и расположенных по смыслу.

### Внедрение в проект

- Скачать репозиторий
- Установить зависимости с помощью `yarn` в корне репозитория (все вложенные package.json будут обработаны автоматически)
- Заменить код в src

# Code style

Настройки линтеров находятся в configs/linters

#### Eslint:

`yarn lint:js:check` - вывод только ошибок eslint, не включая ‘warnings’

`yarn lint:js:check:all` - проверка линтеров в обычном режиме, вывод предупреждений и ошибок

`yarn lint:js:fix` - автоисправление некоторых ошибок

#### Prettier:

`yarn format:check` - проверка соответствия форматирования

`yarn format:fix` - автоформатирование

#### Stylelint - проверка стилизации

`yarn lint:css:check` - проверка

`yarn lint:css:fix` - автоисправление

##### Пример интеграции с WebStorm:

- Prettier:

file ⇒ settings ⇒ настройки ⇒ apply

![Prettier settings](https://goo.su/aKEI)

- Eslint:

![Esllint settings](https://goo.su/b8eI)

При включенной автопроверке IDE предложит исправить некоторые найденные ошибки

![Autofix](https://goo.su/9Oe4)

В случае, если в коде есть какие-то ошибки линта, то приложение не будет скомпилировано без устранения ошибок

![Error](https://goo.su/aJ1s)
