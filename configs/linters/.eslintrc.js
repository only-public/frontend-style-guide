const path = require('path');

const rootDirectory = path.resolve(__dirname, '../..');

module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    tsconfigRootDir: rootDirectory,
    project: './tsconfig.json',
    createDefaultProgram: true,
  },
  globals: {
    JSX: true,
  },
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  plugins: ['babel', 'import', 'react', 'prettier', 'sonarjs'],
  extends: [
    'react-app',
    'react-app/jest',
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'airbnb',
    'plugin:sonarjs/recommended',
  ],
  rules: {
    '@typescript-eslint/explicit-function-return-type': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/prefer-default-export': 'off',
    'import/no-cycle': 'warn',
    'import/no-mutable-exports': 'warn',
    'import/extensions': 'off',
    'import/no-unresolved': 'off',
    'no-unused-vars': 'off',
    'linebreak-style': 'off', // Неправильно работает в Windows.
    'implicit-arrow-linebreak': 'off',
    'arrow-parens': 'off', // Несовместимо с prettier
    'object-curly-newline': 'off', // Несовместимо с prettier
    'no-mixed-operators': 'off', // Несовместимо с prettier
    'arrow-body-style': 'off',
    'function-paren-newline': 'off', // Несовместимо с prettier
    'no-plusplus': 'off',
    'space-before-function-paren': 'off', // Несовместимо с prettier
    'no-prototype-builtins': 'off',
    'no-underscore-dangle': 'off',
    'class-methods-use-this': 'off',
    indent: 'off',
    'no-continue': 'off',
    'operator-linebreak': 'off',
    'max-classes-per-file': 'off',
    'consistent-return': 'off',
    'no-bitwise': 'off',
    'no-unused-expressions': 'error',
    'prefer-const': 'warn',
    'no-useless-escape': 'warn',
    'no-restricted-syntax': 'warn',
    'no-confusing-arrow': 'error',
    'no-return-assign': 'warn',
    eqeqeq: 'warn',
    'dot-notation': 'warn',
    'array-callback-return': 'warn',
    'no-await-in-loop': 'warn',
    'no-use-before-define': 'off',
    'guard-for-in': 'warn',
    'prefer-template': 'warn',
    'no-nested-ternary': 'error',
    'new-cap': 'warn',
    'no-void': 'off',
    'no-useless-return': 'warn',
    strict: 'warn',
    quotes: 'warn',
    'prefer-object-spread': 'warn',
    'prefer-arrow-callback': 'off',
    'comma-dangle': 'off',
    camelcase: 'off', // есть в typescript

    'max-len': ['error', 150, 2, { ignoreUrls: true }], // airbnb позволяет некоторые пограничные случаи
    'no-console': 'warn', // airbnb использует предупреждение
    'no-alert': 'error', // airbnb использует предупреждение
    'no-shadow': 'off',

    'no-param-reassign': 'off',

    'react/static-property-placement': 'off',
    'react/forbid-prop-types': 'off',
    'react/require-default-props': 'warn',
    'react/no-unused-prop-types': 'off',
    'prefer-destructuring': 'off',
    'wrap-iife': 'off',

    'react/no-find-dom-node': 'off',
    'react/no-did-mount-set-state': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'react/no-this-in-sfc': 'warn',
    'react/prop-types': 'off',
    'react/jsx-boolean-value': 'off',
    'react/destructuring-assignment': 'off',
    'react/jsx-filename-extension': 'off',
    'react/jsx-curly-newline': 'off',
    'react/no-array-index-key': 'off',
    'react/jsx-wrap-multilines': 'off',
    'react/prefer-stateless-function': 'off',
    'react/no-children-prop': 'off',
    'react/jsx-props-no-spreading': 'off',
    'react/no-danger': 'off',

    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/unbound-method': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/no-unnecessary-type-assertion': 'off',
    '@typescript-eslint/class-name-casing': 'off',
    '@typescript-eslint/restrict-template-expressions': 'off', // null/undefined автоматически преобразуются в строки
    // не нужно прописывать возвращаемое значение функции, если тс сам его выводит (засорение кода)
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-floating-promises': 'off',

    'jsx-a11y/click-events-have-key-events': 'off',
    'jsx-a11y/label-has-associated-control': 'off',
    'jsx-a11y/media-has-caption': 'off',
    'jsx-a11y/no-static-element-interactions': 'off',
    'jsx-a11y/no-noninteractive-element-interactions': 'off',
    'jsx-a11y/control-has-associated-label': 'off',

    'sonarjs/no-duplicate-string': 'off',
    'sonarjs/no-identical-functions': 'off',
    'sonarjs/prefer-immediate-return': 'off',
    'prettier/prettier': [
      'error',
      {
        endOfLine: 'auto',
      },
    ],
  },
  overrides: [
    {
      files: ['*.d.ts'],
      rules: {
        'no-empty-pattern': 'off',
      },
    },
    {
      files: ['*.test.*'],
      rules: {
        'no-undef': 'off',
      },
    },
  ],
};
